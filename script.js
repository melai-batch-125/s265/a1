
db.users.insertMany(
		[
		{
			"firstName":"Diane",
			"lastName":"Murphy",
			"email":"dmurphy@mail.com",
			"isAdmin": false,
			"isActive": true
		},
		{
			"firstName":"Mary",
			"lastName":"Patterson",
			"email":"mpatterson@mail.com",
			"isAdmin": false,
			"isActive": true

		},
		{
			"firstName":"Jeff",
			"lastName":"Firrelli",
			"email":"jfirrelli@mail.com",
			"isAdmin": false,
			"isActive": true

		},
		{
			"firstName":"Gerard",
			"lastName":"Bondur",
			"email":"gbondur@mail.com",
			"isAdmin": false,
			"isActive": true

		},
		{
			"firstName":"Pamela",
			"lastName":"Castillo",
			"email":"pcastillo@mail.com",
			"isAdmin": true,
			"isActive": false

		},
		{
			"firstName":"George",
			"lastName":"Vanauf",
			"email":"gvanauf@mail.com",
			"isAdmin": true,
			"isActive": true

		}

		]



	);


db.courses.insertMany(
		[
		{
			"name":"Professional Development",
			"price":10000
		},
		{
			"name":"Business Processing",
			"price":13000

		}


		]



	);



db.courses.updateMany(
	{
		"name": "Professional Development"
	},
	{
		$set:{
		"enrollees":
		[
		
			{"_id":ObjectId("612680f3dc701bffbb97bd12")},
			{"_id":ObjectId("612680f3dc701bffbb97bd14")}
		
		]
	}
	}

	);



db.courses.updateMany(
	{
		"name": "Business Processing"
	},
	{
		$set:{
		"enrollees":
		[
		
			{"_id":ObjectId("612680f3dc701bffbb97bd15")},
			{"_id":ObjectId("612680f3dc701bffbb97bd13")}
		
		]
	}
	}

	);


db.users.find(
	{
		"isAdmin": false
	}


	);

db.users.find();
db.courses.find();
